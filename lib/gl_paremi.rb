# frozen_string_literal: true

require_relative "gl_paremi/version"

module GlParemi
  class Error < StandardError; end
  # Your code goes here...
end
