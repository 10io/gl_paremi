# frozen_string_literal: true

require 'gitlab'

module GlParemi
  module Gitlab
    class Registry
      def initialize(project_id:, endpoint:, token:, package_type:)
        @project_id = project_id
        @endpoint = endpoint
        @token = token
        @package_type = package_type
      end

      def packages
        @packages ||= gitlab_client.project_packages(@project_id, package_type: @package_type).auto_paginate.map do |package|
          Package.new(package.name, package.version)
        end
      end

      def remove_package(name:, version:)
        pkg = gitlab_client.project_packages(@project_id, package_type: @package_type, package_name: name).auto_paginate.find do |package|
          package.version == version
        end

        if pkg
          gitlab_client.delete_project_package(@project_id, pkg.id)
          Config.logger.info("Deleted package #{name} - #{version} from GitLab instance #{@endpoint}")
        end
      end

      private

      def gitlab_client
        @client ||= ::Gitlab.client(endpoint: @endpoint, private_token: @token || ENV.fetch('GITLAB_API_PRIVATE_TOKEN', nil))
      end
    end
  end
end
