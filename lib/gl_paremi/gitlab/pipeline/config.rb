module GlParemi
  module Gitlab
    module Pipeline
      class Config
        def self.no_op_add_config
          self.new
              .stages(['add'])
              .add_job(
                name: 'nothing:to:add',
                stage: 'add',
                script: "echo 'no packages to add'"
              )
        end

        def self.no_op_remove_config
          self.new
              .stages(['remove'])
              .add_job(
                name: 'nothing:to:remove',
                stage: 'remove',
                script: "echo 'no packages to remove'"
              )
        end

        def initialize
          @config = {}
        end

        def image(image)
          @config['image'] = image

          self
        end

        def stages(stages)
          @config['stages'] = stages

          self
        end

        def cache(cache_definition)
          @config['cache'] = cache_definition

          self
        end

        def add_job(name:, stage:, script:, variables: {})
          @config[name] = {
            'stage' => stage,
            'variables' => variables,
            'script' => script
          }

          self
        end

        def to_yaml
          @config.to_yaml
        end
      end
    end
  end
end
