# frozen_string_literal: true

require 'thor'

module GlParemi
  module Commands
    module Registries
      def target
        target_config = Config.options['target_type']

        case target_config.keys.first
        when 'gitlab'
          build_gitlab_registry_from(target_config['gitlab'])
        else
          raise ArgumentError, "Unsupported target_type: #{target_type}"
        end
      end

      def build_gitlab_registry_from(config)
        Gitlab::Registry.new(project_id: config['project_id'], endpoint: config['endpoint'], token: config['token'], package_type: :npm)
      end
    end
  end
end
