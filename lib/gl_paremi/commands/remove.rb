# frozen_string_literal: true

require 'thor'

module GlParemi
  module Commands
    class Remove < Thor

      namespace :remove

      desc 'npm', 'Remove from an NPM package registry'
      method_option :help, aliases: '-h', type: :boolean, desc: 'Display usage information'
      method_option :config_path, aliases: '-c', type: :string, desc: 'YAML config path'
      def npm
        if options[:help]
          invoke :help, ['npm']
        else
          raise CLI::Error, 'no config path set.' unless options[:config_path]

          Config.load_from(options[:config_path])

          require_relative 'remove/npm'
          GlParemi::Commands::Remove::Npm.new(options).execute
        end
      end
    end
  end
end
