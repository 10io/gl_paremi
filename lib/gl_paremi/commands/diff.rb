# frozen_string_literal: true

require 'thor'

module GlParemi
  module Commands
    class Diff < Thor

      namespace :diff

      desc 'npm', 'Diff from an NPM package registry'
      method_option :help, aliases: '-h', type: :boolean, desc: 'Display usage information'
      method_option :config_path, aliases: '-c', type: :string, desc: 'YAML config path'
      method_option :verbose, aliases: '-v', type: :boolean, desc: 'Enable verbose log', default: false
      method_option :add_pipeline_config_path, aliases: '-apc', type: :string, desc: 'Pipeline config path for the add operations'
      method_option :remove_pipeline_config_path, aliases: '-rpc', type: :string, desc: 'Pipeline config path for the remove operations'
      def npm
        if options[:help]
          invoke :help, ['npm']
        else
          raise CLI::Error, 'no config path set.' unless options[:config_path]

          Config.load_from(options[:config_path])

          require_relative 'diff/npm'
          GlParemi::Commands::Diff::Npm.new(options).execute
        end
      end
    end
  end
end
