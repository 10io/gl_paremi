# frozen_string_literal: true

require_relative '../../command'
require 'json'
require 'net/http'
require 'tty-command'
require 'open-uri'

module GlParemi
  module Commands
    class Add
      class Npm < GlParemi::Command
        def initialize(options)
          @options = options
        end

        def execute
          download_tar_gz
          configure_npm
          npm_push
        end

        private

        def npm_push
          cmd = TTY::Command.new
          cmd.run("npm publish pkg.tar.gz")
        end

        def download_tar_gz
          download = URI.open(tar_gz_url)
          IO.copy_stream(download, 'pkg.tar.gz')
        end

        def configure_npm
          cmd = TTY::Command.new
          cmd.run("npm config set registry #{target_registry_url}")
          host = target_registry_url.gsub('https:', '').gsub('http:', '')
          cmd.run("npm config set -- '#{host}:_authToken' \"${CI_JOB_TOKEN}\"")
        end

        def target_registry_url
          @target_registry_url ||= "#{target_endpoint}/projects/#{target_project_id}/packages/npm/"
        end

        def target_endpoint
          Config.options.dig('target_type', target_type_key, 'endpoint')
        end

        def target_project_id
          Config.options.dig('target_type', target_type_key, 'project_id')
        end

        def target_type_key
          Config.options['target_type'].keys.first
        end

        def source_type_key
          Config.options['source_type'].keys.first
        end

        def tar_gz_url
          endpoint = Config.options.dig('source_type', source_type_key, 'endpoint') || 'https://registry.npmjs.org'

          url = "#{endpoint}/#{ENV['PACKAGE_NAME']}"
          metadata = JSON.parse(Net::HTTP.get(URI(url)))

          metadata&.dig('versions', ENV['PACKAGE_VERSION'], 'dist', 'tarball')
        end
      end
    end
  end
end
