# frozen_string_literal: true

require_relative '../../command'

module GlParemi
  module Commands
    class Diff
      class Npm < GlParemi::Command
        include Registries

        def initialize(options)
          @options = options
        end

        def execute
          diff = Differ.new(from: source, to: target, verbose: @options[:verbose]).execute

          return unless @options[:add_pipeline_config_path] || @options[:remove_pipeline_config_path]

          generate_pipeline_configurations(diff)
        end

        private

        def generate_pipeline_configurations(diff)
          if @options[:add_pipeline_config_path]
            config_yaml = generate_add_config(diff).to_yaml
            File.write(@options[:add_pipeline_config_path], config_yaml)
            Config.logger.info "Add pipeline config written in #{@options[:add_pipeline_config_path]}"

            if @options[:verbose]
              Config.logger.info(config_yaml)
            end
          end

          if @options[:remove_pipeline_config_path]
            config_yaml = generate_remove_config(diff).to_yaml
            File.write(@options[:remove_pipeline_config_path], config_yaml)
            Config.logger.info "Remove pipeline config written in #{@options[:remove_pipeline_config_path]}"

            if @options[:verbose]
              Config.logger.info(config_yaml)
            end
          end
        end

        def generate_add_config(diff)
          return Gitlab::Pipeline::Config.no_op_add_config unless diff[:add].any?

          config = Gitlab::Pipeline::Config.new
                                              .stages(['add'])
                                              .image(Config.options_ci_add_image)
                                              .cache(Config.options_ci_add_cache)
          diff[:add].each do |package|
            config.add_job(
              name: "add:#{package.name}:#{package.version}",
              stage: 'add',
              variables: variables_from(package),
              script: Config.options_ci_add_script
            )
          end
          config
        end

        def generate_remove_config(diff)
          return Gitlab::Pipeline::Config.no_op_remove_config unless diff[:remove].any?

          config = Gitlab::Pipeline::Config.new
                                              .stages(['remove'])
                                              .image(Config.options_ci_remove_image)
                                              .cache(Config.options_ci_remove_cache)
          diff[:remove].each do |package|
            config.add_job(
              name: "remove:#{package.name}:#{package.version}",
              stage: 'remove',
              variables: variables_from(package),
              script: Config.options_ci_remove_script
            )
          end
          config
        end

        def variables_from(package)
          {
            'PACKAGE_NAME' => package.name,
            'PACKAGE_VERSION' => package.version
          }
        end

        def source
          source_config = Config.options['source_type']

          case source_config.keys.first
          when 'npmjs'
            NPM::Registry.new
          when 'gitlab'
            build_gitlab_registry_from(source_config['gitlab'])
          else
            raise ArgumentError, "Unsupported source_type: #{source_type}"
          end
        end
      end
    end
  end
end
