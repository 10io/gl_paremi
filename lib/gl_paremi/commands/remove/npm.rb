# frozen_string_literal: true

require_relative '../../command'

module GlParemi
  module Commands
    class Remove
      class Npm < GlParemi::Command
        include Registries

        def initialize(options)
          @options = options
        end

        def execute
          target.remove_package(name: ENV['PACKAGE_NAME'], version: ENV['PACKAGE_VERSION'])
        end
      end
    end
  end
end
