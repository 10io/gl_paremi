# frozen_string_literal: true

require 'yaml'
require 'tty-logger'

module GlParemi
  class Config
    def self.load_from(path)
      @path = path
      @load_from ||= YAML.safe_load(File.read(path))
      validate!(path)
    end

    def self.path
      @path
    end

    def self.[](key)
      @load_from[key]
    end

    def self.logger
      @logger ||= TTY::Logger.new
    end

    def self.npm_package_names
      @package_names ||= (npm.keys || [])
    end

    def self.validate!(path)
      config = options

      raise ArgumentError, "No config section in #{path}" unless config

      source_type = config['source_type']
      raise ArgumentError, "No config/source_type section in #{path}" unless source_type
      raise ArgumentError, "Wrong keys in config/source_type section in #{path}" unless source_type.keys.size == 1

      target_type = config['target_type']
      raise ArgumentError, "No config/target_type section in #{path}" unless target_type
      raise ArgumentError, "Wrong keys in config/target_type section in #{path}" unless target_type.keys.size == 1
    end

    def self.options
      self['config']
    end

    def self.npm
      self['npm']
    end

    def self.options_ci_add_image
      options.dig('ci', 'add', 'image')
    end

    def self.options_ci_add_cache
      options.dig('ci', 'add', 'cache')
    end

    def self.options_ci_add_script
      options.dig('ci', 'add', 'script')
    end

    def self.options_ci_remove_image
      options.dig('ci', 'remove', 'image')
    end

    def self.options_ci_remove_cache
      options.dig('ci', 'remove', 'cache')
    end

    def self.options_ci_remove_script
      options.dig('ci', 'remove', 'script')
    end
  end
end
