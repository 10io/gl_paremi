# frozen_string_literal: true

module GlParemi
  class Package
    attr_reader :name, :version

    def initialize(name, version)
      @name = name
      @version = version
    end
  end
end
