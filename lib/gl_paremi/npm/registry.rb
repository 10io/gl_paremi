# frozen_string_literal: true

module GlParemi
  module NPM
    class Registry
      def initialize
        @base_url = 'https://registry.npmjs.org'
      end

      def packages
        @packages ||= Config.npm_package_names.flat_map do |name|
          url = "#{@base_url}/#{name}"
          metadata = JSON.parse(Net::HTTP.get(URI(url)))

          versions(name, metadata).map do |version|
            Package.new(name, version)
          end
        end
      end

      private

      def versions(name, metadata)
        versions = metadata['versions'].keys

        from = versions.index(min_version_for(name))
        to = versions.index(max_version_for(name) || versions.last)

        versions[from..to]
      end

      def min_version_for(name)
        Config.npm[name]['from']
      end

      def max_version_for(name)
        Config.npm[name]['to']
      end
    end
  end
end
