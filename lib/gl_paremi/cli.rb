# frozen_string_literal: true

require 'thor'
require_relative 'config'
require_relative 'gitlab/registry'
require_relative 'gitlab/pipeline/config'
require_relative 'npm/registry'
require_relative 'commands/registries'
require_relative 'package'

module GlParemi
  # Handle the application command line parsing
  # and the dispatch to various command objects
  #
  # @api public
  class CLI < Thor
    # Error raised by this runner
    Error = Class.new(StandardError)

    desc 'version', 'gl_paremi version'
    def version
      require_relative 'version'
      puts "v#{GlParemi::VERSION}"
    end
    map %w[--version -v] => :version

    require_relative 'commands/remove'
    register GlParemi::Commands::Remove, 'remove', 'remove [SUBCOMMAND]', 'Remove from an NPM package registry'

    require_relative 'commands/add'
    register GlParemi::Commands::Add, 'add', 'add [SUBCOMMAND]', 'Add from an NPM package registry'

    require_relative 'commands/diff'
    require_relative 'differ'
    register GlParemi::Commands::Diff, 'diff', 'diff [SUBCOMMAND]', 'Computes a diff between two package registries'

    def self.exit_on_failure?
      true
    end
  end
end
