# frozen_string_literal: true

require 'tty-table'

module GlParemi
  class Differ
    def initialize(from:, to:, verbose: false)
      @from = from
      @to = to
      @verbose = verbose
    end

    def execute
      result = {
        add: packages_to_add,
        remove: packages_to_remove
      }

      output(result) if @verbose

      result
    end

    private

    def output(result)
      table = TTY::Table.new(
        ['operation', 'package name', 'package version'],
        result[:add].map { |pkg| ['add', pkg.name, pkg.version] } +
        result[:remove].map { |pkg| ['remove', pkg.name, pkg.version] }
      )

      Config.logger.info 'Packages diff:'
      Config.logger.info table.render
    end

    def packages_to_add
      to_add = (source_package_names - target_package_names)
      result = @from.packages.select { |package| to_add.include?(package.name) }

      result << (source_package_names & target_package_names).map(&method(:package_versions_to_add_for))

      result.tap(&:flatten!)
    end

    def packages_to_remove
      to_remove = (target_package_names - source_package_names)
      result = @to.packages.select { |package| to_remove.include?(package.name) }

      result << (source_package_names & target_package_names).map(&method(:package_versions_to_remove_for))

      result.tap(&:flatten!)
    end

    def package_versions_to_add_for(package_name)
      (source_versions_for(package_name) - target_versions_for(package_name)).map do |version|
        # not so great
        Package.new(package_name, version)
      end
    end

    def package_versions_to_remove_for(package_name)
      (target_versions_for(package_name) - source_versions_for(package_name)).map do |version|
        # not so great
        Package.new(package_name, version)
      end
    end

    def source_package_names
      @source_package_names ||= @from.packages.map(&:name).compact
    end

    def target_package_names
      @target_package_names ||= @to.packages.map(&:name).compact
    end

    def source_versions_for(package_name)
      @from.packages.select { |pkg| pkg.name == package_name }.map(&:version)
    end

    def target_versions_for(package_name)
      @to.packages.select { |pkg| pkg.name == package_name }.map(&:version)
    end
  end
end
