RSpec.describe "`gl_paremi add npm` command", type: :cli do
  it "executes `gl_paremi add help npm` command successfully" do
    output = `gl_paremi add help npm`
    expected_output = <<-OUT
Usage:
  gl_paremi add npm

Options:
  -h, [--help], [--no-help]        # Display usage information
  -c, [--config-path=CONFIG_PATH]  # YAML config path

Add from an NPM package registry
    OUT

    expect(output).to eq(expected_output)
  end
end
