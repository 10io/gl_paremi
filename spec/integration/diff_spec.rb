RSpec.describe "`gl_paremi diff` command", type: :cli do
  it "executes `gl_paremi help diff` command successfully" do
    output = `gl_paremi help diff`
    expected_output = <<-OUT
Commands:
  gl_paremi diff help [COMMAND]  # Describe subcommands or one specific subcommand
  gl_paremi diff npm             # Diff from an NPM package registry

OUT

    expect(output).to eq(expected_output)
  end
end
