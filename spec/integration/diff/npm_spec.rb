RSpec.describe "`gl_paremi diff npm` command", type: :cli do
  it "executes `gl_paremi diff help npm` command successfully" do
    output = `gl_paremi diff help npm`
    expected_output = <<-OUT
Usage:
  gl_paremi diff npm

Options:
  -h, [--help], [--no-help]                                          # Display usage information
  -c, [--config-path=CONFIG_PATH]                                    # YAML config path
  -v, [--verbose], [--no-verbose]                                    # Enable verbose log
  -apc, [--add-pipeline-config-path=ADD_PIPELINE_CONFIG_PATH]        # Pipeline config path for the add operations
  -rpc, [--remove-pipeline-config-path=REMOVE_PIPELINE_CONFIG_PATH]  # Pipeline config path for the remove operations

Diff from an NPM package registry
    OUT

    expect(output).to eq(expected_output)
  end
end
