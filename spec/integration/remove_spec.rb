RSpec.describe "`gl_paremi remove` command", type: :cli do
  it "executes `gl_paremi help remove` command successfully" do
    output = `gl_paremi help remove`
    expected_output = <<-OUT
Commands:
  gl_paremi remove help [COMMAND]  # Describe subcommands or one specific subcommand
  gl_paremi remove npm             # Remove from an NPM package registry

OUT

    expect(output).to eq(expected_output)
  end
end
