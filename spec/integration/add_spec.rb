RSpec.describe "`gl_paremi add` command", type: :cli do
  it "executes `gl_paremi help add` command successfully" do
    output = `gl_paremi help add`
    expected_output = <<-OUT
Commands:
  gl_paremi add help [COMMAND]  # Describe subcommands or one specific subcommand
  gl_paremi add npm             # Add from an NPM package registry

  OUT

    expect(output).to eq(expected_output)
  end
end
