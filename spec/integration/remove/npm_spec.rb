RSpec.describe "`gl_paremi remove npm` command", type: :cli do
  it "executes `gl_paremi remove help npm` command successfully" do
    output = `gl_paremi remove help npm`
    expected_output = <<-OUT
Usage:
  gl_paremi remove npm

Options:
  -h, [--help], [--no-help]        # Display usage information
  -c, [--config-path=CONFIG_PATH]  # YAML config path

Remove from an NPM package registry
    OUT

    expect(output).to eq(expected_output)
  end
end
