## :fire: The problem

We get often asked if we have a solution to move around Packages to or from the [GitLab Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/) in an automated way.

At the time of this writing, we don't have anything but we started [investigating](https://gitlab.com/gitlab-org/gitlab/-/issues/298726) this feature.

During that investigation, the implementation approach looked really similar to a pipeline with jobs. Basically, an import operation would be splitted in steps and each step would run in parallel if possible.

This triggered a What If?

What if we use a package format that allow pushing a package from a local file and we don't that in a pipeline to "copy" a package between 2 registries?

This project is the collection of scripts used on pipelines for that.

## :mag: Scope

* Package format supported: NPM.
* Source Package Registry supported: any NPM compliant source or npmjs.org.
* Target Package Registry supported: GitLab.
* The user **MUST** specify which packages (with version) should be imported.

The scope is very small in purpose as this is a prototype/exploration project.

## :bulb: The idea

The logic flow is the following:

1. Establish the state of the package registry on the source and target side.
   * eg. basically a list of available packages with name+version.
1. Create a diff that will update the target and put it in the same state as the source.
   * List of packages to add
   * List of packages to remove
1. Leverage [dynamic child pipelines](https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html#dynamic-child-pipelines) to generate as many jobs as necessary to implement the diff.
1. Watch the child pipelines running and profit!

## :construction_site: This project

This project is basically a collection of 3 commands:

* A command to generate a diff and then generate the appropriate child pipelines config files.
* A command to add a package to the source, eg. download the package from the source and push it to the target.
* A command to remove a package from the source.


This project is not meant for production use at all as it is more a showcase of what is possible to do today.

## :crystal_ball: Follow up thoughts

What's nice with having a pipeline handling the import is that:

* Each operation is its own job = we have a status on it. Very easy to spot which steps failed.
* Each operation can be retried easily (rerun the job).
* Notifications are baked in (you get notified if the pipeline fails).
* This is already possible today. I used what is available on gitlab.com.
* Want to have a mirror feature where periodically, the state of the source is copied to the target? That's downright trivial with [scheduled pipelines](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).
   * Set up a daily execution and :boom: you have a daily mirroring feature.
* We could expand the config so that users have even more filters/tools to select the right package names+version.

Limitations:

* The Package format package manager API needs to have:
   * metadata endpoints.
   * commands to upload a package from a local file.
* I'm wondering how this could be done for Maven as Maven packages are a "set" of files instead of a single one.
* Also: what about package formats that don't work with files?

## :popcorn: Demo

Cool, where is my demo? :popcorn:

I'm glad you asked, it's [here](https://gitlab.com/10io/gl_pkgs_reflector). Check the [pipelines](https://gitlab.com/10io/gl_pkgs_reflector/-/pipelines).
